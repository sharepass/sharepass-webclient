// @dart=2.9
import 'package:flutter/material.dart';
import 'package:sharepass_webclient/password_manager/view/pages/project_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
    @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Passager',
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: ProjectPage()
    );
  }
}