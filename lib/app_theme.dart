import 'package:flutter/material.dart';

class AppTheme {
  AppTheme._();

  static MaterialColor mainColor = Colors.deepOrange;

  static MaterialColor secondaryColor = Colors.blueGrey;

  static MaterialColor greyColor = Colors.grey;

  static Color get lightMainColor => mainColor.shade200;

  static Color get lightSecondaryColor => secondaryColor.shade200;

  static Color get darkMainColor => mainColor.shade800;

  static Color get darkSecondaryColor => secondaryColor.shade800;

  static Color get lightestGreyColor => greyColor.shade50;

  static double mediumSized = 16;

  static double bigSized = 24;

  static double biggerSized = 32;

  static double biggestSized = 40;

  static FontWeight fat = FontWeight.w700;

  static FontWeight regular = FontWeight.w400;
  
  static FontWeight light = FontWeight.w200;

  static FontWeight lightest = FontWeight.w100;
}