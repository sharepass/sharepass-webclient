import 'package:sharepass_webclient/password_manager/domain/entities/password.dart';
import 'package:sharepass_webclient/core/failure/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:sharepass_webclient/password_manager/domain/interfaces/password_manager_repository.dart';

class PasswordManagerRepositoryImpl implements PasswordManagerRepository {
  final PasswordManagerRemoteDataSource remoteDataSource;

  PasswordManagerRepositoryImpl({
    required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, List<Password>>> getProjectPasswords(project) {
    var result = remoteDataSource.getProjectPasswordsFromDatabase(project);
    return result;
  }
}

abstract class PasswordManagerRemoteDataSource {
  Future<Either<Failure, List<Password>>> getProjectPasswordsFromDatabase(any);
}