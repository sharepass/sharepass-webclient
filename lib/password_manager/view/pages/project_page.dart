import 'package:flutter/material.dart';
import 'package:sharepass_webclient/custom_app_bar.dart';
import 'package:sharepass_webclient/password_manager/view/components/organisms/crumb.dart';
import 'package:sharepass_webclient/password_manager/view/components/organisms/project_bar.dart';
import 'package:sharepass_webclient/password_manager/view/sections/project_data_section.dart';

class ProjectPage extends StatelessWidget {
  const ProjectPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: Column(
        verticalDirection: VerticalDirection.up,
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(top: 40),
              child: Column(
                children: [
                  ProjectBar(),
                  Expanded(
                    child: ProjectDataSection()
                  )
                ],
              ),
            )
          ),
          Crumb(),
        ],
      ),
    );
  }
}