import 'package:flutter/material.dart';
import 'package:sharepass_webclient/app_theme.dart';

class TeamCrumbText extends StatelessWidget {
  const TeamCrumbText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text('Dev'.toUpperCase(), style: TextStyle(fontSize: AppTheme.mediumSized, color: AppTheme.lightestGreyColor, fontWeight: AppTheme.lightest));
  }
}