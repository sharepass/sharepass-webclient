import 'package:flutter/material.dart';
import 'package:sharepass_webclient/app_theme.dart';

class CategoryCrumbText extends StatelessWidget {
  const CategoryCrumbText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text('/ Application /'.toUpperCase(), style: TextStyle(fontSize: AppTheme.mediumSized, color: AppTheme.darkSecondaryColor, fontWeight: AppTheme.regular));
  }
}