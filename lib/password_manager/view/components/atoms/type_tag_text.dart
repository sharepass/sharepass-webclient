import 'package:flutter/material.dart';

class TypeTagText extends StatelessWidget {
  const TypeTagText({
    Key? key,
    required this.label,
    required this.isActive,
    required this.color,
  }) : super(key: key);

  final String label;
  final bool isActive;
  final MaterialColor color;

  @override
  Widget build(BuildContext context) {
    return Text(label.toUpperCase(), style: TextStyle(color: isActive ? Colors.grey[50] : color[200], fontSize: 18));
  }
}