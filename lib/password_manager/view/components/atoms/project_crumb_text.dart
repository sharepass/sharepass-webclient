import 'package:flutter/material.dart';
import 'package:sharepass_webclient/app_theme.dart';

class ProjectCrumbText extends StatelessWidget {
  const ProjectCrumbText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text('Memolidays', style: TextStyle(fontSize: AppTheme.biggestSized, color: AppTheme.mainColor, fontWeight: AppTheme.lightest));
  }
}