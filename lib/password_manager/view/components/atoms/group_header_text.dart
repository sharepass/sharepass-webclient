import 'package:flutter/material.dart';

class GroupHeaderText extends StatelessWidget {
  const GroupHeaderText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text('App mobile'.toUpperCase(), style: TextStyle(color: Colors.grey[50], fontSize: 20, fontWeight: FontWeight.w100));
  }
}