import 'package:flutter/material.dart';
import 'package:sharepass_webclient/password_manager/view/components/atoms/type_tag_text.dart';

class TypeTag extends StatelessWidget {
  final String label;
  final bool isActive;
  final MaterialColor color;

  const TypeTag({
    Key? key,
    required this.label,
    required this.isActive,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30),
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
      decoration: BoxDecoration(
        border: Border.all(color: isActive ? color : color.shade200),
        color: isActive ? color : Colors.transparent,
      ),
      child: TypeTagText(label: label, isActive: isActive, color: color)
    );
  }
}