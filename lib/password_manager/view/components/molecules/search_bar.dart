import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class SearchBar extends StatelessWidget {
  const SearchBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      height: 36,
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.blueGrey.shade400),
      ),
      child: Container(
        height: 36,
        width: 36,
        color: Colors.blueGrey[400],
        child: Icon(LineIcons.search, color: Colors.grey[50])
      )
    );
  }
}