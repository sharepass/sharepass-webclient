import 'package:flutter/material.dart';
import 'package:sharepass_webclient/password_manager/view/components/atoms/group_header_text.dart';

class GroupHeaderMinified extends StatelessWidget {
  final MaterialColor color;
  final bool isActive;

  const GroupHeaderMinified({
    required this.color,
    this.isActive = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: color[isActive ? 400 : 200],
        borderRadius: BorderRadius.only(topRight: Radius.circular(16), topLeft: Radius.circular(16))
      ),
      margin: EdgeInsets.only(right: 3),
      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 26),
      transform: Matrix4.translationValues(0, 0, 0),
      child: GroupHeaderText()
    );
  }
}