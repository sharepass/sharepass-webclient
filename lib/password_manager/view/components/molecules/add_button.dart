import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class AddButton extends StatelessWidget {
  const AddButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 30, right: 40),
      child: Icon(LineIcons.plus, size: 28, color: Colors.blueGrey[800])
    );
  }
}