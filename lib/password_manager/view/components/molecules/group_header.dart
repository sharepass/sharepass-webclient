import 'package:flutter/material.dart';
import 'package:sharepass_webclient/password_manager/view/components/atoms/group_header_text.dart';

class GroupHeader extends StatelessWidget {
  const GroupHeader({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.purple[400],
        borderRadius: BorderRadius.only(topRight: Radius.circular(16))
      ),
      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 26),
      transform: Matrix4.translationValues(0, 0, 0),
      child: GroupHeaderText()
    );
  }
}