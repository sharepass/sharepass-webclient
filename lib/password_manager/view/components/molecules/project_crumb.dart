import 'package:flutter/material.dart';
import 'package:sharepass_webclient/password_manager/view/components/atoms/project_crumb_text.dart';

class ProjectCrumb extends StatelessWidget {
  const ProjectCrumb({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 25),
      padding: EdgeInsets.only(bottom: 12),
      child: ProjectCrumbText(),
    );
  }
}