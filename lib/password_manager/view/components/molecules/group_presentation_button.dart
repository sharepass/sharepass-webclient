import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class GroupPresentationButton extends StatelessWidget {
  final bool isMinified;
  const GroupPresentationButton({
    required this.isMinified,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 30, right: 40),
      child: Icon(LineIcons.archive, size: 28, color: Colors.blueGrey[800])
    );
  }
}