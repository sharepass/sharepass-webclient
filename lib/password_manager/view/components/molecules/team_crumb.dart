import 'package:flutter/material.dart';
import 'package:sharepass_webclient/password_manager/view/components/atoms/team_crumb_text.dart';

class TeamCrumb extends StatelessWidget {
  const TeamCrumb({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80,
      height: 80,
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 15),
      decoration: BoxDecoration(
        color: Colors.blueGrey[900],
        borderRadius: BorderRadius.circular(300)
      ),
      transform: Matrix4.translationValues(-25, 0, 0),
      child: TeamCrumbText()
    );
  }
}