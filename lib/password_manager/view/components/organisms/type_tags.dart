import 'package:flutter/material.dart';
import 'package:sharepass_webclient/password_manager/view/components/molecules/type_tag.dart';

class TypeTags extends StatelessWidget {
  const TypeTags({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          TypeTag(label: 'ftp', color: Colors.red, isActive: true),
          TypeTag(label: 'database', color: Colors.green, isActive: false),
          TypeTag(label: 'administration', color: Colors.blue, isActive: false),
        ],
      )
    );
  }
}