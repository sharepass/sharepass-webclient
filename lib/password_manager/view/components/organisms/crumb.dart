import 'package:flutter/material.dart';
import 'package:sharepass_webclient/password_manager/view/components/atoms/category_crumb_text.dart';
import 'package:sharepass_webclient/password_manager/view/components/molecules/project_crumb.dart';
import 'package:sharepass_webclient/password_manager/view/components/molecules/search_bar.dart';
import 'package:sharepass_webclient/password_manager/view/components/molecules/team_crumb.dart';

class Crumb extends StatelessWidget {
  const Crumb({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Expanded(
            child: Row(
              children: [
                TeamCrumb(),
                CategoryCrumbText(),
                ProjectCrumb()
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 20),
            child: SearchBar()
          )
        ],
      ),
    );
  }
}