import 'package:flutter/material.dart';
import 'package:sharepass_webclient/password_manager/view/components/organisms/type_tags.dart';

class ProjectBar extends StatelessWidget {
  const ProjectBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(color: Colors.blueGrey.shade200),
          bottom: BorderSide(color: Colors.blueGrey.shade200)
        )
      ),
      height: 80,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TypeTags(),
        ],
      )
    );
  }
}