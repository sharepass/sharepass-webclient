import 'package:flutter/material.dart';
import 'package:sharepass_webclient/password_manager/view/components/molecules/group_header.dart';
import 'package:sharepass_webclient/password_manager/view/components/organisms/group_data_table.dart';

class Group extends StatelessWidget {
  const Group({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 60),
      child: Column(
        verticalDirection: VerticalDirection.up,
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GroupDataTable(),
          GroupHeader(),
        ],
      )
    );
  }
}