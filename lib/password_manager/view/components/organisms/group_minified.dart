import 'package:flutter/material.dart';
import 'package:sharepass_webclient/password_manager/view/components/molecules/group_header_minified.dart';
import 'package:sharepass_webclient/password_manager/view/components/organisms/group_data_table_minified.dart';

class GroupMinified extends StatelessWidget {
  const GroupMinified({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 60),
      child: Column(
        verticalDirection: VerticalDirection.up,
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GroupDataTableMinified(),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              GroupHeaderMinified(color: Colors.blue,),
              GroupHeaderMinified(color: Colors.purple, isActive: true,),
            ],
          ),
        ],
      )
    );
  }
}