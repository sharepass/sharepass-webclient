import 'package:flutter/material.dart';
import 'package:sharepass_webclient/password_manager/view/components/organisms/group.dart';

class ProjectData extends StatelessWidget {
  const ProjectData({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 75),
      child: SingleChildScrollView(
        child: 
        // GroupMinified(),
        Column(
          children: [
            Group(),
            Group(),
            Group(),
            Group(),
          ],
        ),
      )
    );
  }
}