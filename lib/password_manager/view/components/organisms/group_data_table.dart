import 'package:flutter/material.dart';
import 'package:sharepass_webclient/custom_data_cell.dart';

class GroupDataTable extends StatelessWidget {
  const GroupDataTable({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(color: Colors.purple.shade400, width: 6),
          top: BorderSide(color: Colors.purple.shade400, width: 2),
          bottom: BorderSide(color: Colors.purple.shade200),
          right: BorderSide(color: Colors.purple.shade200),
        )
      ),
      child: DataTable(
        headingRowHeight: 0,
        columns: [
          DataColumn(label: Container()),
          DataColumn(label: Container()),
          DataColumn(label: Container()),
          DataColumn(label: Container()),
          DataColumn(label: Container()),
          DataColumn(label: Container()),
          DataColumn(label: Container()),
        ],
        rows: [
          DataRow(
            color: MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
              return Colors.red.withOpacity(0.05);  // Use the default value.
            }),
            cells: [
              DataCell(Text('ftp'.toUpperCase(), style: TextStyle(color: Colors.red, fontSize: 16))),
              DataCell(CustomDataCell.password(Colors.red)),
              DataCell(CustomDataCell.user('Pepito', Colors.red)),
              DataCell(CustomDataCell.email('johnny-depp@hotmail.com', Colors.red)),
              DataCell(CustomDataCell.url('https://pass.applilogik.com', Colors.red)), 
              DataCell(CustomDataCell.custom('8080', Colors.red, 'port')),  
              DataCell(ActionsCell(false, Colors.red)),  
            ]
          ),
          DataRow(
            color: MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
              return Colors.green.withOpacity(0.05);  // Use the default value.
            }),
            cells: [
              DataCell(Text('database'.toUpperCase(), style: TextStyle(color: Colors.green, fontSize: 16))),
              DataCell(CustomDataCell.password(Colors.green)),
              DataCell(CustomDataCell.user('jdepp45', Colors.green)),
              DataCell(EmptyCell()),  
              DataCell(CustomDataCell.url('https://www.google.fr', Colors.green)),
              DataCell(CustomDataCell.custom('memolidays_db', Colors.green, 'db')),  
              DataCell(ActionsCell(true, Colors.green)),  
            ]
          ),
        ]
      ),
    );
  }
}