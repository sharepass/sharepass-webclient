import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:sharepass_webclient/app_theme.dart';
import 'package:sharepass_webclient/password_manager/view/components/molecules/add_button.dart';
import 'package:sharepass_webclient/password_manager/view/components/molecules/group_presentation_button.dart';
import 'package:sharepass_webclient/password_manager/view/components/organisms/project_data.dart';

class ProjectDataSection extends StatelessWidget {
  const ProjectDataSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          ProjectData(),
          Container(
            alignment: Alignment.topRight,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                GroupPresentationButton(isMinified: true),
                AddButton(),
              ],
            )
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: LeftMenu()
          )
        ],
      ),
    );
  }
}

class LeftMenu extends StatefulWidget {
  const LeftMenu({
    Key? key,
  }) : super(key: key);

  @override
  _LeftMenuState createState() => _LeftMenuState();
}

class _LeftMenuState extends State<LeftMenu> {
  bool _isMinified = true;

  _toggleMenu() {
    setState(() {
      _isMinified = !_isMinified;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: 320,
      child: Stack(
        alignment: Alignment.bottomLeft,
        children: [
          AnimatedOpacity(
            duration: Duration(milliseconds: 500),
            opacity: _isMinified ? 0 : 1,
            child: AnimatedContainer(
              duration: Duration(milliseconds: 500),
              curve: Curves.easeInOut,
              padding: EdgeInsets.only(bottom: 90),
              decoration: BoxDecoration(
                border: Border(right: BorderSide(color: AppTheme.lightSecondaryColor)),
              ),
              transform: Matrix4.translationValues(_isMinified ? -320 : 0, 0, 0),
              child: ListView(
                children: [
                  ExpansionTile(
                    title: Text('APPLICATION'),
                    childrenPadding: EdgeInsets.only(left: 20),
                    children: [
                      ExpansionTile(
                        title: Text('MEMOLIDAYS', style: TextStyle(fontWeight: AppTheme.fat)),
                        children: [
                          ListTile(
                            title: Text('App Mobile', style: TextStyle(color: AppTheme.secondaryColor)),
                            leading: Icon(LineIcons.angle_right, color: AppTheme.secondaryColor, size: 16),
                          ),
                          ListTile(
                            title: Text('App Desktop', style: TextStyle(color: AppTheme.lightSecondaryColor)),
                            leading: Container(width: 12),
                          ),
                          ListTile(
                            title: Text('App Web', style: TextStyle(color: AppTheme.lightSecondaryColor)),
                            leading: Container(width: 12),
                          ),
                          ListTile(
                            title: Text('Backend', style: TextStyle(color: AppTheme.lightSecondaryColor)),
                            leading: Container(width: 12),
                          ),
                        ],
                      ),
                      ExpansionTile(
                        title: Text('BEMOBILE'),
                        children: [
                          ListTile(
                            title: Text('App Mobile')
                          )
                        ],
                      )
                    ],
                  ),
                  ExpansionTile(
                    title: Text('AGENCE WEB')
                  ),
                ],
              )
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 20, left: 20),
            child: IconButton(
              onPressed: () => _toggleMenu(),
              icon: Icon(_isMinified ? LineIcons.angle_right : LineIcons.angle_left, size: 28, color: Colors.blueGrey[800]))
          ),
        ],
      )
    );
  }
}