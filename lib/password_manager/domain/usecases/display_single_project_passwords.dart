import 'package:sharepass_webclient/core/failure/failure.dart';
import 'package:sharepass_webclient/password_manager/domain/entities/password.dart';
import 'package:sharepass_webclient/password_manager/domain/entities/project.dart';
import 'package:sharepass_webclient/password_manager/domain/interfaces/password_manager_presenter.dart';
import 'package:sharepass_webclient/password_manager/domain/interfaces/password_manager_repository.dart';
import 'package:dartz/dartz.dart';

class DisplaySingleProjectPasswords {
  final PasswordManagerRepository repository;
  final PasswordManagerPresenter presenter;

  DisplaySingleProjectPasswords({
    required this.repository, 
    required this.presenter,
  });

  Future<void> call(Project project) async {
    Either<Failure, List<Password>> passwords = await repository.getProjectPasswords(project);
    passwords.fold(
      (failure) => presenter.displayError(failure.message), 
      (passwordList) => presenter.displayPasswords(passwordList),
    );
  }
}