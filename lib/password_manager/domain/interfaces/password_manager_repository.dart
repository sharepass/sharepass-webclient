import 'package:sharepass_webclient/core/failure/failure.dart';
import 'package:sharepass_webclient/password_manager/domain/entities/password.dart';
import 'package:sharepass_webclient/password_manager/domain/usecases/display_single_project_passwords.dart';
import 'package:dartz/dartz.dart';

abstract class PasswordManagerRepository {
  Future<Either<Failure, List<Password>>> getProjectPasswords(project);
}