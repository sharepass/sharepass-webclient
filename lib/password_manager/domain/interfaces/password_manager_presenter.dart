import 'package:sharepass_webclient/password_manager/domain/entities/password.dart';

abstract class PasswordManagerPresenter {
  displayPasswords(List<Password> passwords);
  displayError(String failureMessage);
}
