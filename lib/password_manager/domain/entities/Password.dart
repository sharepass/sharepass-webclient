class Password {
  final int id;
  final String value;

  Password({
    required this.id,
    required this.value,
  });
}
