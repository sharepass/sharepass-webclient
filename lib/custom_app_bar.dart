import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:sharepass_webclient/app_theme.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => Size.fromHeight(60);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        Container(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: Icon(LineIcons.users, color: AppTheme.darkSecondaryColor)
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: Icon(LineIcons.cog, color: AppTheme.darkSecondaryColor)
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: Icon(LineIcons.user, color: AppTheme.darkSecondaryColor)
              ),
            ],
          )
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 28, vertical: 6),
          decoration: BoxDecoration(
            color: Colors.blueGrey[900],
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20), bottomRight: Radius.circular(20))
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('Pass'.toUpperCase(), style: TextStyle(fontSize: 18, color: Colors.grey[50], fontWeight: FontWeight.w100)),
              Text('ager'.toUpperCase(), style: TextStyle(fontSize: 18, color: Colors.deepOrange, fontWeight: FontWeight.w500)),
            ],
          ),
        ),
      ],
    );
  }
}
