import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class CustomDataCell extends StatelessWidget {
  final String text;
  final IconData? icon;
  final String type;
  final MaterialColor color;

  const CustomDataCell(this.text, this.icon, {Key? key, this.type = '', this.color = Colors.blueGrey}) : super(key: key);

  CustomDataCell.user(String text, MaterialColor color) : this(text, LineIcons.user, color: color);

  CustomDataCell.password(MaterialColor color) : this('', LineIcons.key, color: color);

  CustomDataCell.email(String text, MaterialColor color) : this(text, LineIcons.at, color: color);

  CustomDataCell.url(String text, MaterialColor color) : this(text, LineIcons.external_link, color: color);

  CustomDataCell.custom(String text, MaterialColor color, String type) : this(' · ' + text, null, color: color, type: type);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(right: 5),
            child: Icon(icon, size: text == '' ? 20 : 16,  color: color[900])
          ),
          Row(
            children: [
              Text(type.toUpperCase(), style: TextStyle(color: color[900], fontWeight: FontWeight.w100)),
              Text(text, style: TextStyle(color: color[900])),
            ],
          )
        ],
      ),
    );
  }
}

class EmptyCell extends StatelessWidget {
  const EmptyCell({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class ActionsCell extends StatelessWidget {
  final bool active;
  final MaterialColor color;

  const ActionsCell(this.active, this.color, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 90,
      child: active
      ? Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Icon(LineIcons.key, size: 20, color: color),
          Icon(LineIcons.edit, size: 20, color: color),
          Icon(LineIcons.envelope, size: 20, color: color),
        ],
      )
      : Container()
    );
  }
}