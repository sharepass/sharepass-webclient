// @dart=2.9

// Maybe make some usecase public fields so I can test them ?
// and then test everything from there ?

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:sharepass_webclient/core/failure/failure.dart';
import 'package:sharepass_webclient/password_manager/domain/entities/password.dart';
import 'package:sharepass_webclient/password_manager/domain/entities/project.dart';
import 'package:sharepass_webclient/password_manager/domain/interfaces/password_manager_presenter.dart';
import 'package:sharepass_webclient/password_manager/domain/interfaces/password_manager_repository.dart';
import 'package:sharepass_webclient/password_manager/domain/usecases/display_single_project_passwords.dart';

import '../../../dummies.dart';

class MockRepository extends Mock 
  implements PasswordManagerRepository {}

class MockPresenter extends Mock 
  implements PasswordManagerPresenter {}

void main() {
  DisplaySingleProjectPasswords displaySingleProjectPasswords;
  MockRepository mockRepository;
  MockPresenter mockPresenter;

  setUp(() {
    mockRepository = MockRepository();
    mockPresenter = MockPresenter();

    displaySingleProjectPasswords = DisplaySingleProjectPasswords(
      repository: mockRepository,
      presenter: mockPresenter,
    );
  });

  group('Usecase DisplaySingleProjectPasswords', () {
    test('when successful, should display passwords from a project', () async {
      // ARRANGE
      when(mockRepository.getProjectPasswords(any))
          .thenAnswer((_) async => dummyPasswordsOk);

      // ACT
      await displaySingleProjectPasswords(dummyProject);

      // ASSERT
      verify(mockRepository.getProjectPasswords(dummyProject));
      verify(mockPresenter.displayPasswords(dummyPasswordList));

      // ACT
      await displaySingleProjectPasswords(dummyProject2);

      // ASSERT
      verify(mockRepository.getProjectPasswords(dummyProject2));
      verify(mockPresenter.displayPasswords(dummyPasswordList));
    });

    test('when not successful, should display the error', () async {
      // ARRANGE
      when(mockRepository.getProjectPasswords(any))
          .thenAnswer((_) async => dummyPasswordsNok);      
      
      // ACT
      await displaySingleProjectPasswords(dummyProject);
      
      // ASSERT
      verify(mockRepository.getProjectPasswords(dummyProject));
      verify(mockPresenter.displayError(dummyFailure.message));
    });
  });
}
