// @dart=2.9

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:sharepass_webclient/password_manager/data/repositories/password_manager_repository_impl.dart';

import '../../../dummies.dart';

class MockPasswordManagerRemoteDataSource extends Mock 
  implements PasswordManagerRemoteDataSource {}

void main() {
  PasswordManagerRepositoryImpl passwordManagerRepository;
  MockPasswordManagerRemoteDataSource mockRemoteDataSource;

  setUp(() {
    mockRemoteDataSource = MockPasswordManagerRemoteDataSource();

    passwordManagerRepository = PasswordManagerRepositoryImpl(
      remoteDataSource: mockRemoteDataSource,
    );
  });

  group('Repository PasswordManagerRepository', () {
    
    test('when succesful, should return a list of passwords from data source', () async {
      // ARRANGE
      when(mockRemoteDataSource.getProjectPasswordsFromDatabase(any))
        .thenAnswer((_) async => dummyPasswordsOk);
      
      // ACT
      final result = await passwordManagerRepository.getProjectPasswords(dummyProject);
      
      // ASSERT
      expect(result, dummyPasswordsOk);
    });
  });
}