import 'package:dartz/dartz.dart';
import 'package:sharepass_webclient/core/failure/failure.dart';
import 'package:sharepass_webclient/password_manager/domain/entities/password.dart';
import 'package:sharepass_webclient/password_manager/domain/entities/project.dart';

List<Password> dummyPasswordList = [
  Password(id: 1, value: '1234'),
  Password(id: 2, value: '5678'),
];

Failure dummyFailure = Failure();

Either<Failure, List<Password>> dummyPasswordsOk = Right(dummyPasswordList);
Either<Failure, List<Password>> dummyPasswordsNok = Left(dummyFailure);

Project dummyProject = Project(id: 1, name: 'project1');
Project dummyProject2 = Project(id: 2, name: 'project2');

